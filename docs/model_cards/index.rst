.. role:: raw-html-m2r(raw)
   :format: html


模型
==========================

.. toctree::
   :glob:
   :maxdepth: 1

   ../../research/skywork/skywork
   ../../research/baichuan/baichuan
   ../../research/baichuan2/baichuan2
   ../../research/qwen/qwen
   ../../research/qwen1_5/qwen1_5
   llama2
   llama
   glm3
   glm2
   glm
   codegeex2
   ../../research/wizardcoder/wizardcoder
   ../../research/ziya/ziya
   ../../research/internlm/internlm
   bloom
   gpt2
   pangualpha
   bilip2
   clip
   ../../research/visualglm/visualglm
   sam
   t5
   bert
   mae
   swin
   vit
   ../../research/iflytekspark/iflytekspark
