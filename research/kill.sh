#!/bin/bash

# 使用 ps 命令查找 main.py 的进程，并获取 PID 列表
PIDS=$(ps -aux | grep 'run_qwen1_5.py' | grep -v grep | awk '{print $2}')

# 检查是否找到了 PID
if [ -z "$PIDS" ]; then
    echo "未找到 main.py 的进程"
else
    # 遍历 PID 列表，逐个终止进程
    echo "找到 main.py 的进程，PID 列表: $PIDS，正在尝试终止..."
    for PID in $PIDS
    do
        kill -9 $PID
    done
    echo "进程已终止"
fi
